// Require the packages we will use:
var http = require("http"),
socketio = require("socket.io"),
fs = require("fs");

var userlist = {}; //list of all users
var roomlist = {}; //list of all rooms

// Listen for HTTP connections. 
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.

	fs.readFile("chat.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.

		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);


// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	console.log("socket id: " +socket.id);
	//user sign up
	socket.on('signUp', function(data){
		if (data.username in userlist){
			socket.emit('signUp_to_client',{"success":false});
		}else{
			userlist[data.username] = {
				"name": data.username,
				"socket":socket,
				"curRoom":"",
				"ownRoom":false,
				"message":[]
			};
			socket.emit('signUp_to_client',{"username":data.username,"success":true});
			socket.emit('updateRooms',{"roomlist":roomlist});
		}
		
	});
	//user sign out
	socket.on('signOut',function(data){
		delete userlist[data.username];
		if (data.inRoom){
			//delete it from the cur users in room
			socket.leave(data.roomName);
			roomlist[data.roomName].users.splice(roomlist[data.roomName].users.indexOf(data.username), 1);
		}
		//if last user sign out
		if (roomlist[data.roomName]!==undefined){
			if (roomlist[data.roomName].users.length===0){
				destroyRoom(data.roomName);
			}
		}				
		socket.emit('cleanUser',{"username":data.username});
	});
	//create a public chatroom
	socket.on('createRoom_to_server',function(data){
		if (data.roomName in roomlist){
			socket.emit('createRoom_to_client',{"success":false,"isOwner":true});
		}else{

			roomlist[data.roomName] = {
				"name":data.roomName,
				"owner":data.owner,
				"pw": data.pw,
				"users": [],
				"block": []				
			};
			roomlist[data.roomName].users.push(data.owner);

			io.sockets.emit('createRoom_to_client',{"roomlist":roomlist,"success":true,"isOwner":false});
			//also join the owner into the room
			socket.join(data.roomName);
			socket.emit('createRoom_to_client',{"owner":data.owner,"roomlist":roomlist,"success":true,"isOwner":true,"roomName":data.roomName});
		}
	});
	//create a private chatroom
	socket.on('createPublicRoom_to_server',function(data){
		if (data.roomName in roomlist){
			socket.emit('createPublicRoom_to_client',{"success":false,"isOwner":true});
		}else{

			roomlist[data.roomName] = {
				"name":data.roomName,
				"owner":data.owner,
				"pw": "",
				"users": [],
				"block": []				
			};
			roomlist[data.roomName].users.push(data.owner);
			io.sockets.emit('createPublicRoom_to_client',{"roomlist":roomlist,"success":true,"isOwner":false});
			//also join the owner into the room
			socket.join(data.roomName);
			socket.emit('createPublicRoom_to_client',{"owner":data.owner,"roomlist":roomlist,"success":true,"isOwner":true,"roomName":data.roomName});
		}
	});
	//join a public room
	socket.on('joinRoom_to_server',function(data){
		var userWannaJoin = data.username;
		var roomWannaJoin = data.roomName;
		var pswd = data.pw;

		if (roomlist[roomWannaJoin].block.indexOf(userWannaJoin)>(-1)){
			//if the user is blocked by the owner of the room
			socket.emit('joinRoom_to_client',{"success":false,"msg":"Not allowed to join this room.","isUser":true});
		}else if(roomlist[roomWannaJoin].users.indexOf(userWannaJoin)>(-1)){
			//user already in the room
			socket.emit('joinRoom_to_client',{"success":false,"msg":"You've already in this room","isUser":true});
		}else if (pswd!=roomlist[roomWannaJoin].pw){
			//password wrong
			socket.emit('joinRoom_to_client',{"success":false,"msg":"Incorrect Password.","isUser":true});
		}else{
			//allowed to join
			socket.join(data.roomName);
			roomlist[data.roomName].users.push(data.username);			
			io.to(data.roomName).emit('joinRoom_to_client',{"success":true,"curRoomUsers":roomlist[data.roomName].users,"roomName":data.roomName,"isUser":false});
			socket.emit('joinRoom_to_client',{"success":true,"curRoomUsers":roomlist[data.roomName].users,"roomName":data.roomName,"isUser":true});
		}
	});
	//join a private room
	socket.on('joinPublicRoom_to_server',function(data){
		var userWannaJoin = data.username;
		var roomWannaJoin = data.roomName;
		if (roomlist[roomWannaJoin].block.indexOf(userWannaJoin)>(-1)){
				//if the user is blocked by the owner of the room
				socket.emit('joinPublicRoom_to_client',{"success":false,"msg":"Not allowed to join this room.","isUser":true});

			}else if(roomlist[roomWannaJoin].users.indexOf(userWannaJoin)>(-1)){
			//user already in the room
			socket.emit('joinPublicRoom_to_client',{"success":false,"msg":"You've already in this room","isUser":true});
		}else{
		//allowed to join
		socket.join(data.roomName);
		roomlist[data.roomName].users.push(data.username);	
		io.to(data.roomName).emit('joinPublicRoom_to_client',{"success":true,"curRoomUsers":roomlist[data.roomName].users,"roomName":data.roomName,"isUser":false});
		socket.emit('joinPublicRoom_to_client',{"success":true,"curRoomUsers":roomlist[data.roomName].users,"roomName":data.roomName,"isUser":true});

	}
});

	//send message
	socket.on('message_to_server', function(data) {
		if (data.anony){
			io.to(data.room).emit("message_to_client",{"message":data.message,"sender":"Anonymous"});
		}else{
			io.to(data.room).emit("message_to_client",{"message":data.message,"sender":data.sender});
		}
		
	});
	//send private message
	socket.on('private_message_to_server', function(data) {
		socket.emit("message_to_client",{"message":("(private to "+data.receiver+") "+data.message),"sender":data.sender});
		userlist[data.receiver].socket.emit("message_to_client",{"message":("(private to "+data.receiver+") "+data.message),"sender":data.sender});
	});

	//user quit the room
	socket.on('quit_to_server',function(data){
		socket.leave(data.roomName);
		roomlist[data.roomName].users.splice(roomlist[data.roomName].users.indexOf(data.username), 1);
		//owner leaves
		if (roomlist[data.roomName].owner==data.username){
			destroyRoom(data.roomName);
			socket.emit('deleteRoom_to_client',{"success":true,"msg":("Room "+data.roomName+" has been deleted.")});
		}else{
			io.to(data.roomName).emit('quit_to_client',{"username":data.username,"roomName":data.roomName,"usersIn":roomlist[data.roomName].users});
		}
		socket.emit('quitSuccess_to_client',{"roomName":data.roomName});				
		io.sockets.emit('updateRooms',{"roomlist":roomlist});
	});

	//user leaves room
	function leaveRoom(user,room){
		userlist[user].socket.leave(room);
		roomlist[room].users.splice(roomlist[room].users.indexOf(user), 1);
		userlist[user].socket.emit('quitSuccess_to_client',{"roomName":room});
		io.to(room).emit('userLeave_to_client',{"curRoomUsers":roomlist[room].users,"roomName":room});
		io.sockets.emit('updateRooms',{"roomlist":roomlist});
	}
	//room no longer exists
	function destroyRoom(rm){
		for (var i=0;i<roomlist[rm].users.length;i++){
			leaveRoom(roomlist[rm].users[i],rm);
		}
		delete roomlist[rm];
		io.sockets.emit('updateRooms',{"roomlist":roomlist});
	}

	//owner temporarily kicks user
	socket.on('kickUser_to_server',function(data){
		if (roomlist[data.room].users.indexOf(data.kicked)!=(-1)){
			if (data.kicker==roomlist[data.room].owner){				
				leaveRoom(data.kicked,data.room);
				socket.emit('kickUser_to_client',{"success":true,"msg":("User "+data.kicked+" has been kicked out of Room "+data.room+".")});
				userlist[data.kicked].socket.emit('beingKicked_to_client',{"room":data.room,"kicker":data.kicker,"msg":"You have been kicked out of the room!"});
			}else{
				//not owner
				socket.emit('kickUser_to_client',{"success":false,"msg":"You do not own this room."});
			}
		}else{
			//no such user in this room
			socket.emit('kickUser_to_client',{"success":false,"msg":"User does not exist."});
		}		
	});
	//owner permanently bans users
	socket.on('blockUser_to_server',function(data){
		if (roomlist[data.room].users.indexOf(data.blocked)!=(-1)){
			if (data.blocker==roomlist[data.room].owner){
				roomlist[data.room].block.push(data.blocked);//put the user in the block list
				leaveRoom(data.blocked,data.room);
				socket.emit('kickUser_to_client',{"success":true,"msg":("User "+data.blocked+" has been blocked out of Room "+data.room+".")});
				userlist[data.blocked].socket.emit('kickUser_to_client',{"room":data.room,"kicker":data.blocker,"msg":"You have been kicked out of the room!"});
			}else{
				//not owner
				socket.emit('kickUser_to_client',{"success":false,"msg":"You do not own this room."});
			}
		}else{
			//no such user is this room
			socket.emit('kickUser_to_client',{"success":false,"msg":"User does not exist."});
		}		
	});
});
